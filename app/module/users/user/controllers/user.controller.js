const User = require('../models/user.model.js');
const Joi = require('@hapi/joi');
const UserServices = require('../services/user');

const schema = Joi.object().keys({
    firstName: Joi.string().required().messages({
        'string.base': `"fistName" must be a 'string'`,
        'string.empty': `"firstName" cannot be empty`,
        'any.required': `Please enter your "firstName"`
    }),
    lastName: Joi.string().required().messages({
        'string.base': `"lastName" must be a 'string'`,
        'string.empty': `"lastName" cannot be empty`,
        'any.required': `Please enter your "lastName"`
    }),
    username: Joi.string().required().messages({
        'string.base': `"username" must be a 'string'`,
        'string.empty': `"username" cannot be empty`,
        'any.required': `Please enter your "username"`
    }),
    avatar: Joi.string().allow(null, '').optional().messages({
        'string.base': `"avatar" must be a 'string'`
    }),
    gender: Joi.string().required().messages({
        'string.base': `"gender" must have a value like 'LGBT', 'Male' or 'Female'`,
        'string.empty': `"gender" cannot be empty`,
        'any.required': `Please enter your "gender"`
    }),
    dialCode: Joi.string().allow(null, '').optional().messages({
        'string.base': `"dialCode" must be a 'string'`
    }),
    phoneNumber: Joi.string().allow(null, '').optional().messages({
        'string.base': `"phoneNumber" must be a 'string'`
    }),
    facebook: Joi.string().allow(null, '').optional().messages({
        'string.base': `"facebook" must be a 'string'`
    }),
    email: Joi.string().email().allow('').messages({
        'string.base': `"email" must have type 'example@gmail.com'`,
        'string.email': `"email" must have type 'example@gmail.com'`,
        'any.required': `Please enter your email "email"`
    }),
    lgclub_visited: Joi.array().items(Joi.object({
        lgclub_id: Joi.string().required().messages({
            'string.base': `"lgclub_id" must be a 'string'`,
            'string.empty': `"lgclub_id" cannot be empty`,
            'any.required': `Please enter your "lgclub_id"`
        }),
        lgclub_name: Joi.string().required().messages({
            'string.base': `"lgclub_name" must be a 'string'`,
            'string.empty': `"lgclub_name" cannot be empty`,
            'any.required': `Please enter your "lgclub_name"`
        }),
      })).optional().default([]).messages({
        'array.base': `"lgclub_visited" must be an array`
    }),
    user_lgclub_id: Joi.string().allow(null, '').optional().messages({
        'string.base': `"user_lgclub_id" must be a 'string'`
    }),
    password: Joi.string().min(6).required().messages({
        'string.base': `"password" must be a string`,
        'string.min': `"password" must have at least 6 letter`,
        'string.empty': `"password" cannot be empty`,
        'any.required': `Please enter your "password"`
    }),
    address: Joi.string().allow(null, '').optional().messages({
        'string.base': `"address" must be a 'string'`
    }),
    district: Joi.string().allow(null, '').optional().messages({
        'string.base': `"district" must be a 'string'`
    }),
    ward: Joi.string().allow(null, '').optional().messages({
        'string.base': `"ward" must be a 'string'`
    }),
    city: Joi.string().allow(null, '').optional().messages({
        'string.base': `"ward" must be a 'string'`
    }),
    zipCode: Joi.string().allow(null, '').optional().messages({
        'string.base': `"zipCode" must be a 'string'`
    }),
    country: Joi.string().allow(null, '').optional().messages({
        'string.base': `"country" must be a 'string'`
    })
}).unknown();

const updateschema = Joi.object().keys({
    firstName: Joi.string().optional().messages({
        'string.base': `"fistName" must be a 'string'`,
        'string.empty': `"firstName" cannot be empty`
    }),
    lastName: Joi.string().optional().messages({
        'string.base': `"lastName" must be a 'string'`,
        'string.empty': `"lastName" cannot be empty`
    }),
    username: Joi.string().optional().messages({
        'string.base': `"username" must be a 'string'`,
        'string.empty': `"username" cannot be empty`
    }),
    avatar: Joi.string().allow(null, '').optional().messages({
        'string.base': `"avatar" must be a 'string'`
    }),
    gender: Joi.string().required().messages({
        'string.base': `"gender" must have a value like 'LGBT', 'Male' or 'Female'`,
        'string.empty': `"gender" cannot be empty`,
        'any.required': `Please enter your "gender"`
    }),
    dialCode: Joi.string().allow(null, '').optional().messages({
        'string.base': `"dialCode" must be a 'string'`
    }),
    phoneNumber: Joi.string().allow(null, '').optional().messages({
        'string.base': `"phoneNumber" must be a 'string'`
    }),
    facebook: Joi.string().allow(null, '').optional().messages({
        'string.base': `"facebook" must be a 'string'`
    }),
    email: Joi.string().email().optional().messages({
        'string.base': `"email" must have type 'example@gmail.com'`,
        'string.email': `"email" must have type 'example@gmail.com'`,
        'string.empty': `"email" cannot be empty`
    }),
    lgclub_visited: Joi.array().items(Joi.object({
        lgclub_id: Joi.string().required().messages({
            'string.base': `"lgclub_id" must be a 'string'`,
            'string.empty': `"lgclub_id" cannot be empty`,
            'any.required': `Please enter your "lgclub_id"`
        }),
        lgclub_name: Joi.string().required().messages({
            'string.base': `"lgclub_name" must be a 'string'`,
            'string.empty': `"lgclub_name" cannot be empty`,
            'any.required': `Please enter your "lgclub_name"`
        }),
      })).optional().default([]).messages({
        'array.base': `"lgclub_visited" must be an array`
    }),
    user_lgclub_id: Joi.string().allow(null, '').optional().messages({
        'string.base': `"user_lgclub_id" must be a 'string'`
    }),
    password: Joi.string().min(6).optional().messages({
        'string.base': `"password" must be a string`,
        'string.min': `"password" must have at least 6 letter`,
        'string.empty': `"password" cannot be empty`
    }),
    address: Joi.string().allow(null, '').optional().messages({
        'string.base': `"address" must be a 'string'`
    }),
    district: Joi.string().allow(null, '').optional().messages({
        'string.base': `"district" must be a 'string'`
    }),
    ward: Joi.string().allow(null, '').optional().messages({
        'string.base': `"ward" must be a 'string'`
    }),
    city: Joi.string().allow(null, '').optional().messages({
        'string.base': `"ward" must be a 'string'`
    }),
    zipCode: Joi.string().allow(null, '').optional().messages({
        'string.base': `"zipCode" must be a 'string'`
    }),
    country: Joi.string().allow(null, '').optional().messages({
        'string.base': `"country" must be a 'string'`
    })
}).unknown();

exports.createParticipant = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
          return next(res.status(400).send(validate.error));
        }
    
        const data = req.body;
        if(data.role !== null){
            data.role = "participants";
        }
        let avatar = req.files.avatar;
        avatar.mv('./uploads/' + avatar.name);
        data.avatar = avatar.name;
        const user = await UserServices.createNewUser(data);

        if(!user){
            res.status(403).send({
                messages: "Participant already exists"
            });
        }
        else{
            res.send(user);
        }
        return next();
    }catch(err){
        throw err
    }
}

//participants
exports.findUserByToken = async (req, res, next) =>{
    if(req.user == null){
        return res.status(401).send({
            message: "Your login sessions is out of date"
        })
    }else{
        return res.status(200).send(req.user)
    }
}

exports.updateUserByToken = async (req, res, next) =>{
    const validate = updateschema.validate(req.body);
    if (validate.error) {
        return next(res.status(400).send(validate.error));
    }
    if(req.user == null){
        return res.status(404).send({
            message: "Your login sessions is out of date"
        })
    }else{
        User.findByIdAndUpdate(req.user._id, req.body, {new: true} )
        .then(user => {
            res.send(user);
        }).catch(err => {
            return res.status(500).send({
                message: "Error updating this user"
            });
        });
    }
}


exports.findAllUsers = async (req, res, next) =>{
    try {
        if(req.user == null){
            return res.status(404).send({
                message: "Your login sessions is out of date"
            });
        }else{
            if(req.user.role == "admin"){
                User.find()
                .then(users => {
                    return res.send(users);
                }).catch(err => {
                    return res.status(401).send({
                        message: "Some error occurerd, please check your internet or database connection"
                    });
                });
            }else{
                res.status(401).send({
                    message: "You must be a 'admin' to do this action"
                })
            }
        }
    }catch(err){
        throw err;
    }
}

exports.login = async (req, res) =>{
    try{
        const data = {
            username: req.body.username,
            password: req.body.password
        }
        const userlogin = await UserServices.login(data)
        if(!userlogin){
            res.status(401).send({
                messages: "Incorrect username"
            });
        }else if(userlogin.correct_password == false){
            res.status(401).send({
                messages: "Incorrect password"
            });
        }
        else{
            res.status(200).send(
                userlogin
           );
        }
    }catch(err){
        throw err;
    }
}

//lgclub admin
exports.createLanguageClubAdmin = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
          return next(res.status(400).send(validate.error));
        }
    
        const data = req.body;
        if(data.role !== null){
            data.role = "lgclubadmin";
        }
        let avatar = req.files.avatar;
        avatar.mv('./uploads/' + avatar.name);
        data.avatar = avatar.name;
        const user = await UserServices.createNewUser(data);

        if(!user){
            res.status(403).send({
                messages: "Participant already exists"
            });
        }
        else{
            res.send(user);
        }
        return next();
    }catch(err){
        throw err
    }
}