const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const UserSchema = mongoose.Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    username: {
        type: String
    },
    role: {
        type: String,
        enum : ['participants', 'admin', 'lgclubadmin', 'lgclubauthor'],
        default: "participants",
        index: true
    },
    avatar: {
        type: String,
    },
    gender:{
        type: String,
        enum: ['LGBT', 'Male', 'Female'],
        default: "Male",
        index: true
    },
    dialCode:{
        type: String
    },
    phoneNumber: {
        type: String
    },
    facebook:{
        type: String
    },
    email: {
        type: String
    },
    lgclub_visited:[{
        lgclub_id: {
            type: Schema.Types.ObjectId,
            index: true
        },
        lgclub_name:{
            type: String
        }
    }],
    user_lgclub_id: {
        type: Schema.Types.ObjectId,
        index: true
    },
    password:{
        type: String
    },
    address: {
        type: String,
        default: ''
    },
    district: {
        type: String,
        default: ''
    },
    ward: {
        type: String,
        default: ''
    },
    city: {
        type: String,
        default: ''
    },
    zipCode:{
        type: String,
        default: ''
    },
    country:{
        type: String,
        default: ''
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('User', UserSchema);