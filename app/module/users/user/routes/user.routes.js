module.exports = router => {
    const users = require('../controllers/user.controller.js');
    /**
   *    Api create participants
   * */
    router.post('/signup/participants', users.createParticipant);

    /**
   *    Api get all user
   * */
    router.get('/users', Authorization, users.findAllUsers);

     /**
   *    Api get current user
   * */
    router.get('/users/loggedin', Authorization, users.findUserByToken);

     /**
   *    Api update user info
   * */
  router.put('/users/update', Authorization, users.updateUserByToken);

    /**
   *    Api for user login
   * */
    router.post('/users/login', users.login);
    /**
   *    Api get a list of visited user from a specific english club
   * */

   /**
   *    Api create language club admin
   * */
  router.post('/signup/lgclubadmin', users.createLanguageClubAdmin);
    


}