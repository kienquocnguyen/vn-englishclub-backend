const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const EventCommentsSchema = mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    event_id: {
        type: Schema.Types.ObjectId,
        index: true
    },
    reply_id:{
        type: Schema.Types.ObjectId,
        index: true
    },
    event_title:{
        type: String
    },
    type:{
        type: String,
        enum: ['comment', 'reply']
    },
    userFullName:{
        type: String
    },
    userAvatar:{
        type: String
    },
    content: {
        type: String
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('EventComments', EventCommentsSchema);