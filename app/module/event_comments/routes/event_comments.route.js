module.exports = router => {
    const eventComments = require('../controllers/event_comments.controller.js');

    
    router.post('/eventComments', Authorization, eventComments.create);
    router.post('/eventComments/uploadPhoto', Authorization, eventComments.uploadCommentPhoto)
    router.put('/eventComments/update/:commentId', eventComments.updateComment);
    router.get('/event/eventComments', eventComments.findAllCommentsByEvent);
}