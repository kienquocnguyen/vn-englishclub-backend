const EventComments = require('../models/event_comments.model.js');
const Event = require('../../events/models/events.model.js');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));

const schema = Joi.object().keys({
    userId: Joi.string().optional().messages({
        'string.base': `"userId" must be a 'string'`,
        'string.empty': `"userId" cannot be empty`
    }),
    event_id: Joi.string().required().messages({
        'string.base': `"event_id" must be a 'string'`,
        'string.empty': `"event_id" cannot be empty`,
        'any.required': `Please enter your "event_id"`
    }),
    reply_id: Joi.string().optional().messages({
        'string.base': `"reply_id" must be a 'string'`,
        'string.empty': `"reply_id" cannot be empty`
    }),
    event_title: Joi.string().required().messages({
        'string.base': `"event_title" must be a 'string'`,
        'string.empty': `"event_title" cannot be empty`,
        'any.required': `Please enter your "event_title"`
    }),
    type: Joi.string().required().messages({
        'string.base': `"type" must be a 'string'`,
        'string.empty': `"type" must be 'comment' or 'reply'`,
        'any.required': `Please enter your "type"`
    }),
    userFullName: Joi.string().optional().messages({
        'string.base': `"userFullName" need to be upload`,
        'string.empty': `"userFullName" need to be upload`
    }),
    userAvatar: Joi.string().optional().messages({
        'string.base': `"userAvatar" need to be upload`,
        'string.empty': `"userAvatar" need to be upload`
    }),
    content: Joi.string().required().messages({
        'string.base': `"content" must be a 'string'`,
        'string.empty': `"content" cannot be empty`,
        'any.required': `Please enter your "content"`
    })
}).unknown();

const updateschema = Joi.object().keys({
    userId: Joi.string().optional().messages({
        'string.base': `"userId" must be a 'string'`,
        'string.empty': `"userId" cannot be empty`
    }),
    event_id: Joi.string().optional().messages({
        'string.base': `"event_id" must be a 'string'`,
        'string.empty': `"event_id" cannot be empty`,
        'any.required': `Please enter your "event_id"`
    }),
    reply_id: Joi.string().optional().messages({
        'string.base': `"reply_id" must be a 'string'`,
        'string.empty': `"reply_id" cannot be empty`
    }),
    event_title: Joi.string().optional().messages({
        'string.base': `"event_title" must be a 'string'`,
        'string.empty': `"event_title" cannot be empty`,
        'any.required': `Please enter your "event_title"`
    }),
    type: Joi.string().optional().messages({
        'string.base': `"type" must be a 'string'`,
        'string.empty': `"type" must be 'comment' or 'reply'`,
        'any.required': `Please enter your "type"`
    }),
    userFullName: Joi.string().optional().messages({
        'string.base': `"userFullName" need to be upload`,
        'string.empty': `"userFullName" need to be upload`
    }),
    userAvatar: Joi.string().optional().messages({
        'string.base': `"userAvatar" need to be upload`,
        'string.empty': `"userAvatar" need to be upload`
    }),
    content: Joi.string().optional().messages({
        'string.base': `"content" must be a 'string'`,
        'string.empty': `"content" cannot be empty`,
        'any.required': `Please enter your "content"`
    })
}).unknown();


exports.create = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
            return next(res.status(400).send(validate.error));
        }

        if(req.user == null){
            return res.status(404).send({
                message: "Your login sessions is out of date"
            });
        }else{
            const data = req.body;
            data.userFullName = req.user.firstName + " " +  req.user.lastName;
            data.userId = req.user._id;
            data.userAvatar = req.user.avatar;
            const eventComments = new EventComments(data);
            await eventComments.save();
            const totalComments = await EventComments.find({event_id: data.event_id}).countDocuments();
            await Event.update({_id: data.event_id}, {$set: {"totalComment": totalComments}})
            res.status(200).send(eventComments)
        }
    }catch(err){
        throw err;
    }
}

exports.findAllCommentsByEvent = async (req, res, next) =>{
    try {
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }
        const eventComments = await EventComments.find({event_id:  req.query.eventId}).skip(page * limit).limit(limit).sort({ createdAt: -1 })
        if(eventComments.length > 0){
            EventComments.find({event_id:  req.query.eventId}).countDocuments()
            .then((totalComments) =>{
                const total = {
                    "page": page + 1,
                    "limit": limit,
                    "total": totalComments
                };
                const data = {
                    "comments": eventComments,
                    "total": total
                }
                return res.send(data)
            })
        }else{
            return res.status(404).send({
             message: "Sorry, currently we don't have any events match with your searching."
            })
        }
    }catch(err){
        throw err;
    }
}

exports.uploadCommentPhoto = async (req, res, next) =>{
    try{
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).send('No files were uploaded.');
        }
        Object.entries(req.files).forEach((entry) => {
            entry[1].mv('./uploads/' + entry[1].name);
        })
        res.send({
            messages: 'File uploaded!'
        });
    }catch(err){
        throw err;
    }
}

exports.updateComment = async (req ,res ,next) =>{
    const validate = updateschema.validate(req.body);
    if (validate.error) {
        return next(res.status(400).send(validate.error));
    }
    if(req.user == null){
        return res.status(404).send({
            message: "You need a account to make comments."
        })
    }else{
        const data = req.body
        EventComments.findByIdAndUpdate(req.params.commentId, req.body, {new: true} )
        .then(comments => {
            res.send(comments);
        }).catch(err => {
            return res.status(500).send({
                message: "Error updating this user"
            });
        });
    }
}