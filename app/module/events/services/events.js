const Event = require('../models/events.model.js');

exports.create = async (data) => {
    try {
            const events = new Event(data);
            await events.save();
            return events;
    }catch(e){
        throw e;
    }
}