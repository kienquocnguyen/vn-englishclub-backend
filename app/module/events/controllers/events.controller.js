const Event = require('../models/events.model.js');
const LanguageClub = require('../../language_club/models/language_club.model.js');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));
const EventServices = require('../services/events.js');
const moment = require('moment');

const schema = Joi.object().keys({
    authorId: Joi.string().required().messages({
        'string.base': `"authorId" must be a 'string'`,
        'string.empty': `"authorId" cannot be empty`,
        'any.required': `Please enter you "authorId"`
    }),
    lgclub_id: Joi.string().allow(null, '').optional().messages({
        'string.base': `"lgclub_id" must be a 'string'`,
        'string.empty': `"lgclub_id" cannot be empty`,
        'any.required': `Please enter you "lgclub_id"`
    }),
    title: Joi.string().required().messages({
        'string.base': `"title" must be a 'string'`,
        'string.empty': `"title" cannot be empty`,
        'any.required': `Please enter your "title"`
    }),
    description: Joi.string().required().messages({
        'string.base': `"description" must be a 'string'`,
        'string.empty': `"description" cannot be empty`,
        'any.required': `Please enter your "description"`
    }),
    authorAvatar: Joi.string().optional().messages({
        'string.base': `"authorAvatar" need to be upload`,
        'string.empty': `"authorAvatar" need to be upload`
    }),
    avatar: Joi.string().optional().messages({
        'string.base': `"avatar" need to be upload`,
        'string.empty': `"avatar" need to be upload`
    }),
    type: Joi.string().required().messages({
        'string.base': `"type" must have a value like 'daily', 'weekly' or 'special'`,
        'string.empty': `"type" cannot be empty`,
        'any.required': `Please enter your "type"`
    }),
    address: Joi.string().required().messages({
        'string.base': `"address" must be a 'string'`,
        'string.empty': `"address" cannot be empty`,
        'any.required': `Please enter your "address"`
    }),
    ward: Joi.string().required().messages({
        'string.base': `"ward" must be a 'string'`,
        'string.empty': `"ward" cannot be empty`,
        'any.required': `Please enter your "ward"`
    }),
    district: Joi.string().required().messages({
        'string.base': `"district" must be a 'string'`,
        'string.empty': `"district" cannot be empty`,
        'any.required': `Please enter your "district"`
    }),
    city: Joi.string().required().messages({
        'string.base': `"city" must be a 'string'`,
        'string.empty': `"city" cannot be empty`,
        'any.required': `Please enter your "city"`
    }),
    zipCode: Joi.string().allow(null, '').optional().messages({
        'string.base': `"zipCode" must be a 'string'`
    }),
    country: Joi.string().required().messages({
        'string.base': `"country" must be a 'string'`,
        'string.empty': `"country" cannot be empty`,
        'any.required': `Please enter your "country"`
    }),
    day_off: Joi.string().optional().messages({
        'string.base': `"day_off" must be a 'string'`,
        'string.empty': `"day_off" cannot be empty`
    }),
    startDate: Joi.date().required().messages({
        'string.base': `"startDate" must be a date with format 'dd/mm/yy'`,
        'string.empty': `"startDate" cannot be empty`,
        'any.required': `Please enter your "startDate"`
    }),
    expiredDate: Joi.date().required().messages({
        'string.base': `"expiredDate" must be a date with format 'dd/mm/yy'`,
        'string.empty': `"expiredDate" cannot be empty`,
        'any.required': `Please enter your "expiredDate"`
    }),
    totalParticipant: Joi.string().optional().messages({
        'number.base': `"totalParticipant" must be a 'number'`,
        'number.empty': `"totalParticipant" cannot be empty`,
        'any.required': `Please enter your "totalParticipant"`
    }),
    totalLike: Joi.string().optional().messages({
        'number.base': `"totalLike" must be a 'number'`,
        'number.empty': `"totalLike" cannot be empty`,
        'any.required': `Please enter your "totalLike"`
    }),
    totalComment: Joi.string().optional().messages({
        'number.base': `"totalComment" must be a 'number'`,
        'number.empty': `"totalComment" cannot be empty`,
        'any.required': `Please enter your "totalComment"`
    }),
    totalViews: Joi.string().optional().messages({
        'number.base': `"totalViews" must be a 'number'`,
        'number.empty': `"totalViews" cannot be empty`,
        'any.required': `Please enter your "totalViews"`
    })
}).unknown();

exports.create = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
          return next(res.status(400).send(validate.error));
        }
        const data = req.body;
        const todayDate = moment().format("YYYY-MM-DD")


        if(req.user == null){
            return res.status(404).send({
                message: "Your login sessions is out of date"
            });
        }else{
            if(!req.files){
                res.status(400).send({
                    message: "Please upload your avatar"
                });
            }else{
                //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
                let avatar = req.files.avatar;
                //Use the mv() method to place the file in upload directory (i.e. "uploads")
                avatar.mv('./uploads/' + avatar.name);
                data.avatar = avatar.name;
                data.authorAvatar = req.user.avatar;
       

                if(req.user.role == "admin"){
                    if(data.type == 'special'){
                        if(moment().format("YYYY-MM-DD") === moment(data.expiredDate).format("YYYY-MM-DD")){
                            res.status(403).send({
                                message: "Expired date cannot be today"
                            })
                        }else if (moment(data.expiredDate).isBefore(todayDate) == true){
                            res.status(406).send({
                                message: "Expired date cannot be older than today"
                            })
                        }else if(moment(data.startDate).format("YYYY-MM-DD HH:MM:SS") === moment(data.expiredDate).format("YYYY-MM-DD HH:MM:SS")){
                            res.status(405).send({
                                message: "Start date and expired date cannot be the same"
                            })
                        }else{
                            const events = await EventServices.create(data);
                            res.send(events);
                        }
                    }else{
                        const events = await EventServices.create(data);
                        res.send(events);   
                    }
                }else if (req.user.role == 'lgclubadmin'){
                    const lgclub = await LanguageClub.findById(req.user.user_lgclub_id);
                    if(lgclub.length !== 0){
                        if(lgclub.activated == true){
                            if(data.type == 'special'){
                                if(moment().format("YYYY-MM-DD") === moment(data.expiredDate).format("YYYY-MM-DD")){
                                    res.status(403).send({
                                        message: "Expired date cannot be today"
                                    })
                                }else if (moment(data.expiredDate).isBefore(todayDate) == true){
                                    res.status(406).send({
                                        message: "Expired date cannot be older than today"
                                    })
                                }else if(moment(data.startDate).format("YYYY-MM-DD") === moment(data.expiredDate).format("YYYY-MM-DD")){
                                    res.status(405).send({
                                        message: "Start date and expired date cannot be the same"
                                    })
                                }else{
                                    const events = await EventServices.create(data);
                                    res.send(events);
                                }
                            }else{
                                if(moment(data.startDate).format("YYYY-MM-DD HH:MM:SS") === moment(data.expiredDate).format("YYYY-MM-DD HH:MM:SS")){
                                    res.status(405).send({
                                        message: "Start time and expired time cannot be the same"
                                    })
                                }else{
                                    const events = await EventServices.create(data);
                                    res.send(events);   
                                }
                            }
                        }else{
                            res.status(403).send({
                                message: "Sorry, your language club still not activated. Please contact with the admin to get more detail."
                            })
                        }
                    }else{
                        res.status(404).send({
                            message: "You need to create an language club before do this action"
                        })
                    }
                }else{
                    res.status(401).send({
                        message: "You must be a 'lgclubadmin' to do this action"
                    })
                }
            }
        }
        return next();
    }catch(err){
        throw err
    }
}

exports.findAllEvents = async (req, res, next) =>{
    try {

        let query = Helper.populateDbQuery(req.query, {
            array: [ 'startDate', 'district', 'city', 'country' ],
        });
        const sort = {totalParticipant: -1};
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }

        //if startDate has value
        if(req.query.startDate){
            Object.assign(query, {startDate: {"$gte": new Date(req.query && req.query.startDate ? req.query.startDate: '')}});
        }

        const events = await Event.find(query).skip(page * limit).limit(limit)
        .sort(sort)
        .exec();
        if(events.length > 0){
            Event.countDocuments()
            .then((docs) =>{
                const total = {
                    "page": page + 1,
                    "limit": limit,
                    "total": docs
                };
                const data = {
                    "event": events,
                    "total": total
                }
                return res.send(data)
            })
        }else{
            return res.status(404).send({
             message: "Sorry, currently we don't have any events match with your searching."
            })
        }
    }catch(err){
        throw err;
    }
}

exports.findEventById = async (req, res) => {
    try{
        Event.findById(req.params.eventId)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "Cannot find event"
                });            
            }else{
                /*
                console.log("today " + Date.now())
                console.log("event date " + event.startDate.valueOf());
                console.log("expired days " + event.expiredByDay);
                const eventDate = new Date(event.startDate.valueOf() + event.expiredByDay * 24*60*60*1000);
                console.log(eventDate.toLocaleDateString())
                */
               LanguageClub.findById(event.lgclub_id)
               .then(language_club => {
                   return console.log(language_club);
               })
                res.status(200).send(event);
            }
        }).catch(err => {
            if(err) {
                return res.status(404).send({
                    message: "Cannot find event"
                });                
            }
            return res.status(500).send({
                message: "Error. Please check your internet connection"
            });
        });
    }
    catch(err){
        throw err;
    }
};

exports.findEventBySlug = async = (req, res) =>{
    Event.find({slug: req.params.permalink})
    .then(event => {
        if(!event) {
            return res.status(404).send({
                message: "Sorry, we can't find this event."
            });            
        }else{
            res.status(200).send(event);
        }
    }).catch(err => {
        if(err) {
            return res.status(404).send({
                message: "Sorry, we can't find this event."
            });                
        }
        return res.status(500).send({
            message: "Error. Please check your internet connection."
        });
    });
}
