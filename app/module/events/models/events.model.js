const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
const Schema = require('mongoose').Schema;

const EventsSchema = mongoose.Schema({
    authorId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    lgclub_id: {
        type: Schema.Types.ObjectId,
        index: true
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    authorAvatar:{
        type: String
    },
    avatar:{
        type: String
    },
    type:{
        type: String,
        enum : ['daily', 'weekly', 'special'],
        default: "participants",
        index: true
    },  
    address: {
        type: String
    },
    ward: {
        type: String
    },
    district: {
        type: String
    },
    city: {
        type: String
    },
    zipCode:{
        type: String
    },
    country: {
        type: String
    },
    day_off:{
        type: String,
        enum : ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
    },
    startDate:{
        type: Date
    },
    expiredDate:{
        type: Date
    },
    totalParticipant:{
        type: Number
    },
    totalLike:{
        type: Number
    },
    totalComment:{
        type: Number
    },
    totalViews:{
        type: Number
    },
    slug: { type: String, slug: ["title"], unique: true }
}, {
    timestamps: true
});

module.exports = mongoose.model('Event', EventsSchema);