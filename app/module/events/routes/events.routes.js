module.exports = router => {
    const events = require('../controllers/events.controller.js');

    
    router.post('/events', Authorization, events.create);

    router.get('/events/:eventId', events.findEventById);
    
    router.get('/eventdetail/:permalink', events.findEventBySlug);

    router.get('/events', events.findAllEvents);
}