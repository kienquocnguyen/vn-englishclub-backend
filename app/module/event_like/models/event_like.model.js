const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const EventLikeSchema = mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    event_id: {
        type: Schema.Types.ObjectId,
        index: true
    },
    userFullName:{
        type: String
    },
    userAvatar:{
        type: String
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('EventLike', EventLikeSchema);