module.exports = router => {
    const eventLike = require('../controllers/event_like.controller.js');

    router.post('/eventLike', Authorization, eventLike.create);
    router.delete('/eventLike/delete', Authorization, eventLike.deleteLike);
    router.get('/event/eventLike', eventLike.findAllLikesByEvent);
    router.get('/eventLike/checkLiked/:eventId', Authorization , eventLike.checkUserLiked);
}