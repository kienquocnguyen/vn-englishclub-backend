const EventLike = require('../models/event_like.model.js');
const Event = require('../../events/models/events.model.js');
const { $where } = require('../models/event_like.model.js');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));

const schema = Joi.object().keys({
    userId: Joi.string().optional().messages({
        'string.base': `"userId" must be a 'string'`,
        'string.empty': `"userId" cannot be empty`
    }),
    event_id: Joi.string().required().messages({
        'string.base': `"event_id" must be a 'string'`,
        'string.empty': `"event_id" cannot be empty`,
        'any.required': `Please enter your "event_id"`
    }),
    event_title: Joi.string().required().messages({
        'string.base': `"event_title" must be a 'string'`,
        'string.empty': `"event_title" cannot be empty`,
        'any.required': `Please enter your "event_title"`
    }),
    userFullName: Joi.string().optional().messages({
        'string.base': `"userFullName" need to be upload`,
        'string.empty': `"userFullName" need to be upload`
    }),
    userAvatar: Joi.string().optional().messages({
        'string.base': `"userAvatar" need to be upload`,
        'string.empty': `"userAvatar" need to be upload`
    })
}).unknown();

exports.create = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
            return next(res.status(400).send(validate.error));
        }
        if(req.user == null){
            return res.status(401).send({
                message: "You need to login to do this action"
            });
        }else{
            const data = req.body;
            data.userFullName = req.user.firstName + " " +  req.user.lastName;
            data.userId = req.user._id;
            data.userAvatar = req.user.avatar;
            //Check already liked
            const checkExist = await EventLike.find({event_id: data.event_id, userId: data.userId});
            if(checkExist.length > 0){
                res.status(403).send({
                    message: "You have already liked this event."
                })
            }else{
                const eventLike = new EventLike(data);
                await eventLike.save();
                const totalLike = await EventLike.find({event_id: data.event_id}).countDocuments();
                await Event.update({_id: data.event_id}, {$set: {"totalLike": totalLike}})
                res.status(200).send(eventLike)
            }
        }
    }catch(err){
        throw err;
    }
}

exports.findAllLikesByEvent = async (req, res, next) =>{
    try {
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }

        let checkLiked = false;
        if(req.query.userId !== ''){
            const checkExist = await EventLike.find({event_id: req.query.eventId, userId: req.query.userId});
            if(checkExist.length > 0){
                checkLiked = true;
            }else{
                checkLiked = false;
            }
        }else{
            checkLiked = false;
        }

        let eventLike = await EventLike.find({event_id:  req.query.eventId}).sort({ createdAt: -1 });
        if(limit !== 0){
            eventLike = await EventLike.find({event_id:  req.query.eventId}).skip(page * limit).limit(limit).sort({ createdAt: -1 })
        }
        if(eventLike.length > 0){
            EventLike.find({event_id:  req.query.eventId}).countDocuments()
            .then((totalLike) =>{
                const total = {
                    "page": page + 1,
                    "limit": limit,
                    "total": totalLike,
                    "checkLiked": checkLiked
                };
                const data = {
                    "like": eventLike,
                    "total": total
                }
                return res.send(data)
            })
        }else{
            return res.status(404).send({
             message: "Sorry, currently we don't have any events match with your searching."
            })
        }
    }catch(err){
        throw err;
    }
}

exports.checkUserLiked = async (req, res, next) =>{
    try {
        let checkLiked = false;
        if(req.user == null){
            checkLiked = false;
            return res.status(401).send({
                message: "You need to login to do this action"
            });
        }else{
            const checkExist = await EventLike.find({event_id: req.params.eventId, userId: req.user._id});
            if(checkExist.length > 0){
                checkLiked = true;
            }else{
                checkLiked = false;
            }
            return res.status(200).send({
                "checkLiked": checkLiked
            })
        }
    }catch(err){
        throw err;
    }
}

exports.deleteLike = async (req ,res) =>{
    if(req.user == null){
        return res.status(404).send({
            message: "You need to login to do this action."
        })
    }else{
        const eventLike = await EventLike.find({event_id: req.query.eventId, userId: req.query.userId});
        if(eventLike.length > 0){
            const removeEventLike = await EventLike.findByIdAndRemove(eventLike[0]._id)
            if(removeEventLike){
                const totalLike = await EventLike.find({event_id: removeEventLike.event_id}).countDocuments();
                await Event.update({_id: eventLike[0].event_id}, {$set: {"totalLike": totalLike}})
                res.status(200).send(removeEventLike);
            }else{
                return res.status(500).send({
                    message: "Lost internet connection"
                });
            }
        }else{
            return res.status(404).send({
                message: "You haven't like this event."
            })
        }
    }
}