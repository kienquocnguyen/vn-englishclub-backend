module.exports = router => {
    const eventParticipation = require('../controllers/event_participation.controller.js');

    router.post('/eventParticipation', Authorization, eventParticipation.create);
    router.put('/eventParticipation/cancel', Authorization, eventParticipation.cancelParticipation);
    router.get('/event/eventParticipation', eventParticipation.findAllParticipationByEvent);
    router.get('/event/user/eventParticipation', eventParticipation.findAllParticipationByEventAndUser)
    router.get('/eventParticipation/checkParticipated/:eventId', Authorization , eventParticipation.checkUserParticipated);
}