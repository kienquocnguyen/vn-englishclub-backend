const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const EventParticipationSchema = mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    event_id: {
        type: Schema.Types.ObjectId,
        index: true
    },
    userFullName:{
        type: String
    },
    userPhoneNumber:{
        type: String,
    },
    userAddress:{
        type: String,
    },
    userAvatar:{
        type: String
    },
    paymentCheckout:{
        type: Boolean,
        default: false
    },
    checkin:{
        type: Date
    },
    checkout:{
        type: Date
    },
    checkinStatus:{
        type: String,
        enum : ['true', 'false', 'cancel'],
        default: "false",
        index: true
    },
    checkoutStatus:{
        type: String,
        enum : ['true', 'false'],
        default: "false",
        index: true
    },
    cancelReason:{
        type: String,
        default: ""
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('EventParticipation', EventParticipationSchema);