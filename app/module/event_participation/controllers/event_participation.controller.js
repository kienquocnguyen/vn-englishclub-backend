const EventParticipation = require('../models/event_participation.model.js');
const Event = require('../../events/models/events.model.js');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));

const schema = Joi.object().keys({
    userId: Joi.string().optional().messages({
        'string.base': `"userId" must be a 'string'`,
        'string.empty': `"userId" cannot be empty`
    }),
    event_id: Joi.string().required().messages({
        'string.base': `"event_id" must be a 'string'`,
        'string.empty': `"event_id" cannot be empty`,
        'any.required': `Please enter your "event_id"`
    }),
    event_title: Joi.string().required().messages({
        'string.base': `"event_title" must be a 'string'`,
        'string.empty': `"event_title" cannot be empty`,
        'any.required': `Please enter your "event_title"`
    }),
    userFullName: Joi.string().optional().messages({
        'string.base': `"userFullName" need to be upload`,
        'string.empty': `"userFullName" need to be upload`
    }),
    userAvatar: Joi.string().optional().messages({
        'string.base': `"userAvatar" need to be upload`,
        'string.empty': `"userAvatar" need to be upload`
    }),
    userAddress: Joi.string().optional().messages({
        'string.base': `"userAvatar" need to be upload`,
        'string.empty': `"userAvatar" need to be upload`
    })
}).unknown();

const cancelSchema = Joi.object().keys({
    event_id: Joi.string().required().messages({
        'string.base': `"event_id" must be a 'string'`,
        'string.empty': `"event_id" cannot be empty`,
        'any.required': `Please enter your "event_id"`
    }),
    cancelReason: Joi.string().optional().allow(null, '').messages({
        'string.base': `"cancelReason" must be a 'string'`
    })
}).unknown();

exports.create = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
            return next(res.status(400).send(validate.error));
        }
        if(req.user == null){
            return res.status(401).send({
                message: "You need to login to do this action"
            });
        }else{
            const data = req.body;
            data.userFullName = req.user.firstName + " " +  req.user.lastName;
            data.userId = req.user._id;
            data.userAvatar = req.user.avatar;
            data.userAddress = req.user.address + ", ward " + req.user.ward + ", district " + req.user.district + ", " + req.user.city + " city, " + req.user.country;
            //Check already signup
            const checkExist = await EventParticipation.find({event_id: data.event_id, userId: data.userId});
            if(checkExist.length > 0){
                res.status(403).send({
                    message: "You have already signup for this event."
                })
            }else{
                const eventParticipation = new EventParticipation(data);
                await eventParticipation.save();
                const totalParticipant = await EventParticipation.find({event_id: data.event_id}).countDocuments();
                await Event.update({_id: data.event_id}, {$set: {"totalParticipant": totalParticipant}})
                res.status(200).send(eventParticipation)
            }
        }
    }catch(err){
        throw err;
    }
}

exports.findAllParticipationByEvent = async (req, res, next) =>{
    try {
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }

        let checkParticipated = false;
        if(req.query.userId !== ''){
            const checkExist = await EventParticipation.find({event_id: req.query.eventId, userId: req.query.userId});
            if(checkExist.length > 0){
                checkParticipated = true;
            }else{
                checkParticipated = false;
            }
        }else{
            checkParticipated = false;
        }

        let eventParticipation = await EventParticipation.find({event_id:  req.query.eventId}).sort({ createdAt: -1 });
        if(req.query.checkinStatus){
            eventParticipation = await EventParticipation.find({event_id:  req.query.eventId, checkinStatus: req.query.checkinStatus}).sort({ createdAt: -1 });
        }
        if(limit !== 0){
            if(req.query.checkinStatus){
                eventParticipation = await EventParticipation.find({event_id:  req.query.eventId, checkinStatus: req.query.checkinStatus}).skip(page * limit).limit(limit).sort({ createdAt: -1 })
            }else{
                eventParticipation = await EventParticipation.find({event_id:  req.query.eventId}).skip(page * limit).limit(limit).sort({ createdAt: -1 })
            }
        }
        if(eventParticipation.length > 0){
            if(req.query.checkinStatus){
                EventParticipation.find({event_id:  req.query.eventId, checkinStatus: req.query.checkinStatus}).countDocuments()
                .then((totalParticipation) =>{
                    const total = {
                        "page": page + 1,
                        "limit": limit,
                        "total": totalParticipation,
                        "checkParticipated": checkParticipated
                    };
                    const data = {
                        "participation": eventParticipation,
                        "total": total
                    }
                    return res.send(data)
                })
            }else{
                EventParticipation.find({event_id:  req.query.eventId}).countDocuments()
                .then((totalParticipation) =>{
                    const total = {
                        "page": page + 1,
                        "limit": limit,
                        "total": totalParticipation,
                        "checkParticipated": checkParticipated
                    };
                    const data = {
                        "participation": eventParticipation,
                        "total": total
                    }
                    return res.send(data)
                })
            }
        }else{
            return res.status(404).send({
                message: "Sorry, currently we don't have any events match with your searching."
            })
        }
    }catch(err){
        throw err;
    }
}

exports.findAllParticipationByEventAndUser = async (req, res, next) =>{
    try {
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }

        let checkParticipated = false;
        if(req.query.userId !== ''){
            const checkExist = await EventParticipation.find({event_id: req.query.eventId, userId: req.query.userId});
            if(checkExist.length > 0){
                checkParticipated = true;
            }else{
                checkParticipated = false;
            }
        }else{
            checkParticipated = false;
        }

        let eventParticipation = await EventParticipation.find({event_id:  req.query.eventId, userId: req.query.userId}).sort({ createdAt: -1 });
        if(req.query.checkinStatus){
            eventParticipation = await EventParticipation.find({event_id:  req.query.eventId, userId: req.query.userId, checkinStatus: req.query.checkinStatus}).sort({ createdAt: -1 });
        }
        if(limit !== 0){
            if(req.query.checkinStatus){
                eventParticipation = await EventParticipation.find({event_id:  req.query.eventId, userId: req.query.userId, checkinStatus: req.query.checkinStatus}).skip(page * limit).limit(limit).sort({ createdAt: -1 })
            }else{
                eventParticipation = await EventParticipation.find({event_id:  req.query.eventId, userId: req.query.userId}).skip(page * limit).limit(limit).sort({ createdAt: -1 })
            }
        }
        if(eventParticipation.length > 0){
            if(req.query.checkinStatus){
                EventParticipation.find({event_id:  req.query.eventId, userId: req.query.userId, checkinStatus: req.query.checkinStatus}).countDocuments()
                .then((totalParticipation) =>{
                    const total = {
                        "page": page + 1,
                        "limit": limit,
                        "total": totalParticipation,
                        "checkParticipated": checkParticipated
                    };
                    const data = {
                        "participation": eventParticipation,
                        "total": total
                    }
                    return res.send(data)
                })
            }else{
                EventParticipation.find({event_id:  req.query.eventId, userId: req.query.userId}).countDocuments()
                .then((totalParticipation) =>{
                    const total = {
                        "page": page + 1,
                        "limit": limit,
                        "total": totalParticipation,
                        "checkParticipated": checkParticipated
                    };
                    const data = {
                        "participation": eventParticipation,
                        "total": total
                    }
                    return res.send(data)
                })
            }
        }else{
            return res.status(404).send({
                message: "Sorry, currently we don't have any events match with your searching."
            })
        }
    }catch(err){
        throw err;
    }
}

exports.checkUserParticipated = async (req, res, next) =>{
    try {
        let checkParticipated = false;
        if(req.user == null){
            checkParticipated = false;
            return res.status(401).send({
                message: "You need to login to do this action"
            });
        }else{
            const checkExist = await EventParticipation.find({event_id: req.params.eventId, userId: req.user._id});
            if(checkExist.length > 0){
                checkParticipated = true;
            }else{
                checkParticipated = false;
            }
            return res.status(200).send({
                "checkParticipated": checkParticipated
            })
        }
    }catch(err){
        throw err;
    }
}

exports.cancelParticipation = async (req ,res) =>{
    const validate = cancelSchema.validate(req.body);
    if (validate.error) {
        return next(res.status(400).send(validate.error));
    }
    if(req.user == null){
        return res.status(404).send({
            message: "You need to login to do this action."
        })
    }else{
        const data = req.body;
        const checkExist = await EventParticipation.find({event_id: data.event_id, userId: req.user._id});
        if(checkExist.length > 0){
            if(checkExist[0].checkinStatus == "cancel"){
                res.status(403).send({
                    message: "You've already canceled this event"
                })
            }else{
                await EventParticipation.update({event_id: data.event_id}, {$set: {"checkinStatus": "cancel", "cancelReason": data.cancelReason}})
                res.status(200).send({
                    message: "Cancel Success"
                })
            }
        }
        else{
            res.status(404).send({
                message: "You haven't signup this event."
            })
        }
    }
}

exports.deleteParticipation = async (req ,res) =>{
    if(req.user == null){
        return res.status(404).send({
            message: "You need to login to do this action."
        })
    }else{
        const eventParticipation = await EventParticipation.find({event_id: req.query.eventId, userId: req.query.userId});
        if(eventParticipation.length > 0){
            const removeEventParticipation = await EventParticipation.findByIdAndRemove(eventParticipation[0]._id)
            if(removeEventParticipation){
                const totalParticipation = await EventParticipation.find({event_id: removeEventParticipation.event_id}).countDocuments();
                await Event.update({_id: eventParticipation[0].event_id}, {$set: {"totalParticipant": totalParticipation}})
                res.status(200).send(removeEventParticipation);
            }else{
                return res.status(500).send({
                    message: "Lost internet connection"
                });
            }
        }else{
            return res.status(404).send({
                message: "You haven't signup this event."
            })
        }
    }
}