module.exports = router => {
    const language_club = require('../controllers/language_club.controller.js');

    
    router.post('/languageclub', Authorization, language_club.create);
    /**
    * Api get all of language club
    * */
    router.get('/languageclub', language_club.findAllLanguageClub);
    /**
    * Api get one specific language club
    * */
    router.get('/languageclub/:languageclubId', language_club.findLanguageClubById);
    /**
    * Api search english club
    * */
   router.get('/filter/languageclub', language_club.findWithFilter);
}