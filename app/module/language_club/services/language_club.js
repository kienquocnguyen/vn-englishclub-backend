const LanguageClub = require('../models/language_club.model.js');

exports.create = async (data) => {
    try {
            const language_club = new LanguageClub(data);
            const check_exists = LanguageClub.find( { $or: [ { name: language_club.name }, { _id: language_club._id } ] } );
            const language_club_exists = await check_exists;
            if( language_club_exists.length > 0 ){
                return;
            }
            else{
                await language_club.save();
                return language_club;
            }
    }catch(e){
        throw e;
    }
}
