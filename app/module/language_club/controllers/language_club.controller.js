const LanguageClub = require('../models/language_club.model.js');
const User = require('../../../module/users/user/models/user.model.js');
const Joi = require('@hapi/joi');
const LanguageClubServices = require('../services/language_club.js');

const schema = Joi.object().keys({
    ownerId: Joi.string().optional().messages({
        'string.base': `"ownerId" must be a 'string'`,
        'string.empty': `"ownerId" cannot be empty`,
        'any.required': `Please enter you "ownerId"`
    }),
    name: Joi.string().required().messages({
        'string.base': `"name" must be a 'string'`,
        'string.empty': `"name" cannot be empty`,
        'any.required': `Please enter your "name"`
    }),
    description: Joi.string().required().messages({
        'string.base': `"description" must be a 'string'`,
        'string.empty': `"description" cannot be empty`,
        'any.required': `Please enter your "description"`
    }),
    dialCode: Joi.string().allow(null, '').optional().messages({
        'string.base': `"dialCode" must be a 'string'`
    }),
    phoneNumber: Joi.string().allow(null, '').optional().messages({
        'string.base': `"phoneNumber" must be a 'string'`
    }),
    email: Joi.string().email().allow('').messages({
        'string.base': `"email" must have type 'example@gmail.com'`,
        'string.email': `"email" must have type 'example@gmail.com'`,
        'any.required': `Please enter your email "email"`
    }),
    logo: Joi.string().optional().messages({
        'string.base': `"logo" must be a 'string'`,
        'string.empty': `"logo" cannot be empty`
    }),
    featureImage: Joi.string().optional().messages({
        'string.base': `"featureImage" must be a 'string'`,
        'string.empty': `"featureImage" cannot be empty`
    }),
    libary: Joi.array().items(Joi.object({
        photo: Joi.string().optional().messages({
            'string.base': `"photo" must be a 'string'`
        })
      })).optional().default([]).messages({
        'array.base': `"libary" must be an array`
    }),
    
    // branch: Joi.array().items(Joi.object({
    //     branch_name: Joi.string().required().messages({
    //         'string.base': `"branch_name" must be a 'string'`,
    //         'string.empty': `"branch_name" cannot be empty`,
    //         'any.required': `Please enter your "branch_name"`
    //     }),
    //     address: Joi.string().required().messages({
    //         'string.base': `"address" must be a 'string'`,
    //         'string.empty': `"address" cannot be empty`,
    //         'any.required': `Please enter your "address"`
    //     }),
    //     district: Joi.string().required().messages({
    //         'string.base': `"district" must be a 'string'`,
    //         'string.empty': `"district" cannot be empty`,
    //         'any.required': `Please enter your "district"`
    //     }),
    //     ward: Joi.string().required().messages({
    //         'string.base': `"ward" must be a 'string'`,
    //         'string.empty': `"ward" cannot be empty`,
    //         'any.required': `Please enter your "ward"`
    //     }),
    //     city: Joi.string().required().messages({
    //         'string.base': `"city" must be a 'string'`,
    //         'string.empty': `"city" cannot be empty`,
    //         'any.required': `Please enter your "city"`
    //     }),
    //     zipCode: Joi.string().allow(null, '').optional().messages({
    //         'string.base': `"zipCode" must be a 'string'`
    //     }),
    //     country: Joi.string().required().messages({
    //         'string.base': `"country" must be a 'string'`,
    //         'string.empty': `"country" cannot be empty`,
    //         'any.required': `Please enter your "country"`
    //     }),
    //     facebook: Joi.string().allow(null, '').optional().messages({
    //         'string.base': `"facebook" must be a 'string'`
    //     })
    //   })).optional().default([]).messages({
    //     'array.base': `"branch" must be an array`
    // }),
}).unknown();

exports.create = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
          return next(res.status(400).send(validate.error));
        }
        if(req.user.role == null){
            return res.status(404).send({
                message: "Your login sessions is out of date"
            })
        }else if(req.user.role !== 'lgclubadmin'){
            return res.status(401).send({
                message: "You have to be an language club admin to do this action"
            })
        }else{
            const data = req.body;
            if(data.activated !== null){
                data.activated = false;
            }
            //upload image
            let logo = req.files.logo;
            let featureImage = req.files.featureImage;
            logo.mv('./uploads/' + logo.name);
            featureImage.mv('./uploads/' + featureImage.name);
            data.logo = logo.name;
            data.featureImage = featureImage.name;
            //set owner id
            data.ownerId = req.user._id.toString();
            const language_club = await LanguageClubServices.create(data);
            if(!language_club){
                res.status(403).send({
                    messages: "This club already exists"
                });
            }
            else{
                await User.findByIdAndUpdate(req.user._id, { $set: { user_lgclub_id: language_club._id.toString() }}, {new: true} )
                res.send(language_club);
            }
            return next();
        }
    }catch(err){
        throw err
    }
}

exports.findAllLanguageClub = async (req, res, next) =>{
    try {
        LanguageClub.find()
        .then(language_club => {
            return res.send(language_club);
        }).catch(err => {
            return res.status(401).send({
                message: "Some error occurerd, please check your internet or database connection"
            });
        });
    }catch(err){
        throw err;
    }
}

exports.findLanguageClubById = async(req, res, next) => {
    try {
        LanguageClub.findById(req.params.languageclubId)
        .then(language_club => {
            return res.send(language_club);
        }).catch(err => {
            return res.status(500).send({
                message: "Some error occurerd, please check your internet or database connection"
            });
        });
    }catch(err){
        throw err;
    }
}

exports.findWithFilter = async (req, res, next) =>{
    try {
        /*
        const query = { branch: { $elemMatch: { $and: [ {city: req.query.city}, {district: req.query.district}, {address: req.query.address}] }  } };
        */
        const query = Helper.populateDbQuery(req.query, {
            array: [ 'district', 'city', 'country' ],
        });
        const sort = {name: 1};
        const language_club = await LanguageClub.find({ branch: { $elemMatch:  query } })
        .sort(sort)
        .exec();
        if(language_club.length > 0){
            return res.send(language_club)
        }else{
            return res.status(404).send({
                message: "Your club is not exists"
            });
        }
    }catch(err){
        throw err;
    }
}