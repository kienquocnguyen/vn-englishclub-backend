const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const LanguageClubSchema = mongoose.Schema({
    ownerId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    name: {
        type: String
    },
    description: {
        type: String
    },
    dialCode:{
        type: String
    },
    phoneNumber: {
        type: String
    },
    email: {
        type: String
    },
    logo:{
        type: String
    },
    featureImage:{
        type: String
    },
    libary:[{
        photo:{
            type: String
        }
    }],
    // branch:[{
    //     branch_name:{
    //         type: String
    //     },
    //     address: {
    //         type: String
    //     },
    //     district: {
    //         type: String
    //     },
    //     ward: {
    //         type: String
    //     },
    //     city: {
    //         type: String
    //     },
    //     zipCode:{
    //         type: String
    //     },
    //     country: {
    //         type: String
    //     },
    //     facebook:{
    //         type: String
    //     },
    //     featureImage:{
    //         type: String
    //     },
    // }],
    activated: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('LanguageClub', LanguageClubSchema);