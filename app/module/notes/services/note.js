const Note = require('../models/note.model.js');
exports.createNote = async data => {
    try {
        return new Promise(async (resovle, reject) => {
            // Create a Note
            const note = new Note({
                title: data.title || "Untitled Note", 
                content: data.content
            });
            
            note.save()
            .then(data => {
                resovle({
                    notes:{
                        data
                    }
                });
            }).catch(err => {
                reject(err);
            });
        })
    } catch (e) {
      throw e;
    }
};
